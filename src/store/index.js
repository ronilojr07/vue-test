import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import category from './modules/category';
import article from './modules/article';

export const store = new Vuex.Store({
    modules: {
        category,
        article
    }
});