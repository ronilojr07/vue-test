import axios from "axios";

const actions = {
  getArticlesByCategory({ commit }, id) {
    return axios
      .get(`http://localhost:9000/api/category/${id}`)
      .then(({ data }) => {
        commit("setArticles", data);
      });
  },

  searchArticles({ commit }, query) {
    return axios
      .get(`http://localhost:9000/api/search/${query}`)
      .then(({ data }) => {
        commit(
          "setArticlesBySearch",
          data.filter(d => d.title.toLowerCase().includes(query))
        );
      });
  }
};

const mutations = {
  setArticle(state, article) {
    state.article = article;
  },
  setArticles(state, articles) {
    articles = articles.filter(article => article.status === "published");
    state.articles = articles;
  },

  setArticlesBySearch(state, articles) {
    articles = articles.filter(
      article => article.status === "published" || article.status === "archived"
    );
    state.articles = articles;
  }
};

const getters = {
  article: state => state.article,
  articles: state => state.articles
};

const state = {
  article: null,
  articles: []
};

export default {
  state,
  actions,
  mutations,
  getters
};
