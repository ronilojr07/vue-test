import axios from "axios";

const actions = {
  getCategories({ commit }) {
    return axios
      .get("http://localhost:9000/api/categories")
      .then(({ data }) => {
        commit("setCategories", data);
      });
  }
};

const mutations = {
  setCategory(state, category) {
    state.category = category;
  },

  setCategories(state, categories) {
    state.categories = categories
      .filter(category => category.enabled)
      .sort((a, b) => a.order - b.order);
  }
};

const getters = {
  category: state => state.category,
  categories: state => state.categories
};

const state = {
  category: null,
  categories: []
};

export default {
  state,
  actions,
  mutations,
  getters
};
