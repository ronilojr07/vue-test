import Vue from 'vue';
import App from './App.vue';
import Router from 'vue-router';
import VueTimeago from 'vue-timeago'
import VueCarousel from 'vue-carousel'

import { router } from './router'
import { store } from './store'
import { BootstrapVue } from 'bootstrap-vue';

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
	return originalPush.call(this, location).catch(err => err)
};

Vue.use(Router);
Vue.use(BootstrapVue);
Vue.use(VueCarousel);
Vue.use(VueTimeago, {
	name: 'Timeago',
	locale: 'en',
});

new Vue({
	el: '#app',
	router,
	store,
	render: h => h(App)
});